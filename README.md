# azrs-tracking

Projekat u okviru kursa AZRS. Alati se primenjuju na projekat [Monopol](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/03-monopol) koji je razvijen u okviru kursa RS.

## Alati:

- [GDB](https://gitlab.com/AnaNika/azrs-tracking/-/issues/1)

- [Valgrind](https://gitlab.com/AnaNika/azrs-tracking/-/issues/2)

- [Gammaray](https://gitlab.com/AnaNika/azrs-tracking/-/issues/3)

- [Git hooks](https://gitlab.com/AnaNika/azrs-tracking/-/issues/6)

- [Clazy](https://gitlab.com/AnaNika/azrs-tracking/-/issues/4)

- [Docker](https://gitlab.com/AnaNika/azrs-tracking/-/issues/9)

- [GCov](https://gitlab.com/AnaNika/azrs-tracking/-/issues/7)

- [ClangFormat](https://gitlab.com/AnaNika/azrs-tracking/-/issues/5)

- [Meld](https://gitlab.com/AnaNika/azrs-tracking/-/issues/8)

Na projektu smo koristili Git sistem za verzionisanje i GitFlow model granjanja.
